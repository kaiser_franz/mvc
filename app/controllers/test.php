<?php

class Test extends Controller
{
    public function index($name = '') {

        $user = $this->model('User');

        $user->name = $name;

        $this->view('home/test', array('name' => $user->name));

    }

}
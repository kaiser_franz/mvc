<?php

class Controller
{
    // Carga un modelo a partir de su nombre
    public function model($model) {
        require_once('../app/models/' . $model . '.php');

        return new $model();
    }

    public function view($view, $data = array()) {
        require_once('../app/views/'. $view .'.html');
    }

    public function render($view, $params = array()) {

        require '../vendor/autoload.php';

        $loader = new Twig_Loader_Filesystem($_SERVER['DOCUMENT_ROOT'] . '/andres/mvc/app/views');
        $twig   = new Twig_Environment($loader);

        return $twig->render($view . '.html', $params);
    }
}
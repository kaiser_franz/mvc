<?php

class App
{
    
    // Definimos el controlador por defecto y el método que se cargará por defecto en cada controlador
    protected $controller = 'home';
    protected $method     = 'index';

    // En este ejemplo los argumentos se van a poder pasar también por la url. Así, tendremos urls de la forma sitio/controller/method/param1/param2/param3...
    protected $params     = array();

    public function __construct()
    {
        $url = $this->parseUrl(); 

        // La logica sera coger la url, comprobar si el controlador existe y servirlo, comprobar que el metodo existe y mandarlo, etc...

        // En nuestro ejemplo, se comprueba si existe el controlador, si existe se setea, si no se devuelve el controlador por defecto. Quiza lo suyo seria construir un controlador para la page not found, claro.
        if (file_exists('../app/controllers/'.$url[0].'.php')) {
            $this->controller = $url[0];
            unset($url[0]);
        }

        require_once('../app/controllers/'.$this->controller.'.php');

        // en la variable controller guardamos un objeto, en lugar del nombre del controlador. Eso nos permite por ejemplo usar la funcion de php method_exists mas adelante
        $this->controller = new $this->controller;

        // comprobamos si existe un metodo en la url y si existe, se pasa
        if (isset($url[1])) {
            if (method_exists($this->controller, $url[1])) {
                $this->method = $url[1];
                unset($url[1]);
            }
        }

        // ahora cogemos los parametros si existen

        // primero miramos si se ha pasado una url, porque si no peta la funcion array_values

        // La funcion array_values crea un array con todos los valores de otro array (util tambien cuando hay arrays asociativos y queremos pasarlo a no asociativos). Como hemos unset la parte del array $url que corresponde al controlador y la del metodo, lo que nos queda se lo pasamos a params.
        $this->params = $url ? array_values($url) : array();

        // Llamamos al metodo apropiado en el controlador indicado con los parametros que haya
        call_user_func_array(array($this->controller, $this->method), $this->params);
    }

    public function parseUrl()
    {
        // Vamos a hacer para este ejemplo una comprobación mínima, solamente si existe la url, y no nos vamos a meter en otras comprobaciones que habría que hacer en una app seria. 
        // El tema del _GET puede llamar la atención, ya que parecería como si la url fuera a venir como ?url=loquesea, cuando no es así. La razón de usar la superglobal __GET es que vamos a utilizar un .htaccess para reescribir la url y pasarla como un parámetro get.

        if (isset($_GET['url'])) {
            // Lo que hacemos es sanear la url, con los siguientes pasos
            // a) $url = rtrim($_GET['url'], '/');     --> elimina la posible / del final
            // b) filter_var(url, FILTER_SANITIZE_URL)   --> sanea la url
            // c) explode('/', url saneada)     --> creamos un array con cada trocito separado por / (por eso era importante quitar si habia una al final)
            // Puesto todo de una vez:
            return $url = explode('/', filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL));
        }
    }
}